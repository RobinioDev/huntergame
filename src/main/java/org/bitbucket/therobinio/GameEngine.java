package org.bitbucket.therobinio;

import java.util.ArrayList;
import java.util.Random;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class GameEngine {
    

    private ArrayList<Player> players = new ArrayList<>();
    private static Random rnd = new Random();
    
    
    public GameEngine() {
        //start without Command
        //should used from an arena class
    }
    
    public GameEngine(ArrayList<Player> players) {
            this.players = players;
    }
    
    public void startGame() {
        for (Player p: players) {
            p.getInventory().clear();
            for (int i = 0; i <= 8; i++) {
                p.getInventory().setItem( i, items("wool_green"));
            }
        }
        Player rndPlayer = randomPlayer();
        for (int i = 1; i <= 8; i++) {
            rndPlayer.getInventory().setItem(i, items("wool_red"));
        }
        rndPlayer.getInventory().setItem(0, items("shot_bow"));
    }
    
    private Player randomPlayer() {
        
        return players.get(rnd.nextInt(players.size()));
    }
    
    private ItemStack items(String item) {
        ItemStack wool_green = new ItemStack(Material.WOOL, 1, (byte) 5);
        
        ItemStack wool_red = new ItemStack(Material.WOOL, 1, (byte) 14);
        
        ItemStack shotBow = new ItemStack(Material.BOW, 1);
        ItemMeta bowMeta = shotBow.getItemMeta();
        bowMeta.addEnchant(Enchantment.ARROW_INFINITE, 1, true);
        shotBow.setItemMeta(bowMeta);
        
        switch (item) {
            case "wool_green":  return wool_green;
            case "wool_red" : return wool_red;
            case "shot_bow": return shotBow;
        }
        ItemStack freshair = new ItemStack(Material.AIR, 0);
        return freshair;
    }
}
