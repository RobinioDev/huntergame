package org.bitbucket.therobinio.commands;

import org.bukkit.entity.Player;

public interface SubCommand {

    public boolean onCommand(Player player, String [] args);
    
    public String help(Player p);
    
    public String getCommand(Player p);
    
    public String permission();
}
