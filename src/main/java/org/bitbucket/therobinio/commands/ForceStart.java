package org.bitbucket.therobinio.commands;

import java.util.ArrayList;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bitbucket.therobinio.GameEngine;

public class ForceStart implements SubCommand{
    
    //toDo implements the enable and diable function 
    
    @Override
    public boolean onCommand(Player player, String[] args) {
       if(player.hasPermission(permission())) {
        ArrayList<Player> players = new ArrayList<>();
        
        for (Player p:Bukkit.getOnlinePlayers()) {
            if (p.getWorld() == player.getWorld()) {
                p.sendMessage("The Game is starting!");
                players.add(p);
            }
        }
        
        GameEngine engine = new GameEngine(players);
        engine.startGame();
        
        return true;
       }
       return false;
    }

    @Override
    public String help(Player p) {
        if (p.hasPermission(permission())) {
            return "You can start the Game instantly!";
        }
        return "You don't got the permission for that!";
    }
    
    @Override
    public String getCommand(Player p) {   
        if (p.hasPermission(permission())) {
            return "/forcestart";
        }
        return "";
    }

    @Override
    public String permission() {
        return "huntergame.forcestart";
    }
}
