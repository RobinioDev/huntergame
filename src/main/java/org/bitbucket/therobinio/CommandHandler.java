package org.bitbucket.therobinio;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import org.bitbucket.therobinio.commands.CreateArena;
import org.bitbucket.therobinio.commands.DelArena;
import org.bitbucket.therobinio.commands.Disable;
import org.bitbucket.therobinio.commands.Enable;
import org.bitbucket.therobinio.commands.ForceStart;
import org.bitbucket.therobinio.commands.Join;
import org.bitbucket.therobinio.commands.Leave;
import org.bitbucket.therobinio.commands.ListArena;
import org.bitbucket.therobinio.commands.SubCommand;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

public class CommandHandler implements CommandExecutor {

    private Plugin plugin;
    private HashMap < String, SubCommand > commands;
    
    ArrayList<Player> players = new ArrayList<>();
    
    public CommandHandler(Plugin plugin) {
        commands = new HashMap < String, SubCommand > ();
        
        loadCommands();
    }
    
    private void loadCommands() {
        commands.put("createarena", new CreateArena());
        commands.put("delarena", new DelArena());
        commands.put("disable", new Disable());
        commands.put("enable", new Enable());
        commands.put("forcestart", new ForceStart());
        commands.put("join", new Join());
        commands.put("leave", new Leave());
        commands.put("listarena", new ListArena());
    }
    
    @Override
    public boolean onCommand(CommandSender cs, Command cmd, String string, String[] args) {
        
        
        if (!(cs instanceof Player)) {
            cs.sendMessage("Only avialble for Players!");
            return true;
        } 
        Player p = (Player) cs;
        
        if (cmd.getName().equalsIgnoreCase("huntergame")) {
            
            if (args.length == 0) {
                p.sendMessage(ChatColor.GOLD + "HunterGame Plugin" + ChatColor.AQUA + " by TheRobinio");
                return true;
            }
           
            if (args[0].equalsIgnoreCase("help")) {
                help(p);
                return true;
            }
            String sub = args[0];
            ArrayList<String> c = new ArrayList <> ();
            c.addAll(Arrays.asList(args));
            c.remove(0);
            args = (String[]) c.toArray(new String[0]);
            HunterPlugin.logger.info(Integer.toString(args.length));
            if(!(commands.containsKey(sub))) {
                p.sendMessage(ChatColor.RED + "This command isn't supported yet by the HungerGamesPlugin!");
                p.sendMessage(ChatColor.GOLD + "For more information /hg help");
                return true;
            }
            try {
                commands.get(sub).onCommand(p, args);
            } catch (Exception e) {
                p.sendMessage(ChatColor.RED + "Error with Command Executor");
                p.sendMessage(ChatColor.GOLD + "For more information /hg help");
            }
            return true;
        }
        return false;
    }
    
    public void help(Player p) {
        p.sendMessage(ChatColor.GOLD + "---HELP---");
       for (String command : commands.keySet()) {
            try{
               p.sendMessage(ChatColor.GREEN + commands.get(command).getCommand(p) + ChatColor.WHITE + commands.get(command).help(p));
            }catch(Exception e){}
}
    }
    
}
