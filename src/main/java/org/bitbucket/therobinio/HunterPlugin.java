package org.bitbucket.therobinio;

import org.bitbucket.therobinio.events.EntityDamage;
import java.util.logging.Logger;
import org.bitbucket.therobinio.events.BlockBreak;
import org.bitbucket.therobinio.events.BlockPlace;
import org.bitbucket.therobinio.events.BlockTeleport;
import org.bitbucket.therobinio.events.CommandCatch;
import org.bitbucket.therobinio.events.KeepLobbyLoaded;
import org.bitbucket.therobinio.events.SignClickEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class HunterPlugin extends JavaPlugin {
    
    public static Logger logger;
    HunterPlugin g = this;
    
    @Override
    public void onEnable(){
       logger = g.getLogger();

       getServer().getScheduler().scheduleSyncDelayedTask(this, new Startup(), 10);
    }
    
    @Override
    public void onDisable() {
        
    }
    
    class Startup implements Runnable {
        public void run() {
            getCommand("huntergame").setExecutor(new CommandHandler(g));
            
            GameManager.getInstance().setup(g);
            
            PluginManager pm = getServer().getPluginManager();
            pm.registerEvents(new EntityDamage(), g);
            pm.registerEvents(new BlockBreak(), g);
            pm.registerEvents(new BlockPlace(), g);
            pm.registerEvents(new BlockTeleport(), g);
            pm.registerEvents(new CommandCatch(), g);
            pm.registerEvents(new KeepLobbyLoaded(), g);
            pm.registerEvents(new SignClickEvent(), g);
        }
    }
}
